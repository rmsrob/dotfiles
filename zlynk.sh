#!/bin/sh

export dotdir="$HOME/Repos/gitlab.com/rmsrob/dotfiles"

cd $dotdir

# symbolic link for bash
rm ~/.bashrc.d
ln -sv $dotdir/config/bash/bashrc.d ~/.bashrc.d
rm ~/.local/bin/bash-scripts
ln -sv $dotdir/config/bash/bash-scripts ~/.local/bin/bash-scripts
rm ~/.bashrc
ln -sv $dotdir/config/bash/bashrc ~/.bashrc
rm ~/.profile
ln -sv $dotdir/config/bash/profile ~/.profile
rm ~/.dircolors
ln -sv $dotdir/config/bash/dircolors ~/.dircolors
rm ~/.inputrc
ln -sv $dotdir/config/bash/inputrc ~/.inputrc
rm ~/.bash_aliases
ln -sv $dotdir/config/bash/bash_aliases ~/.bash_aliases

rm ~/.gitconfig
cp -iv $dotdir/config/bash/gitconfig ~/.gitconfig

# symbolic link for tmux
rm ~/.tmux.conf
ln -sv $dotdir/config/tmux/tmux.conf ~/.tmux.conf

# symbolic link for lynx
rm ~/.config/lynx/lynx.cfg
ln -sv $dotdir/config/lynx/lynx.cfg ~/.config/lynx/lynx.cfg
rm ~/.config/lynx/lynx.lss
ln -sv $dotdir/config/lynx/lynx.lss ~/.config/lynx/lynx.lss

# copy file for alacritty
rm ~/.config/alacritty/alacritty.yml
cp -iv $dotdir/config/alacritty/alacritty.yml ~/.config/alacritty/alacritty.yml

# symbolic link for vim
rm ~/.vimrc
ln -sv $dotdir/config/vim/vim.vim ~/.vimrc

rm ~/.vim/coc-settings.json
cp -iv $dotdir/config/vim/coc-settings.json ~/.vim/coc-settings.json

rm ~/.config/nvim/settings
ln -sv $dotdir/config/vim/settings ~/.config/nvim/settings
rm ~/.config/nvim/themes
ln -sv $dotdir/config/vim/themes ~/.config/nvim/themes
rm ~/.config/nvim/plug
ln -sv $dotdir/config/vim/plug ~/.config/nvim/plug

rm ~/.vim/settings
ln -sv $dotdir/config/vim/settings ~/.vim/settings
rm ~/.vim/themes
ln -sv $dotdir/config/vim/themes ~/.vim/themes
rm ~/.vim/plug
ln -sv $dotdir/config/vim/plug ~/.vim/plug


