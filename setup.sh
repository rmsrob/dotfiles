## source this with . setup.sh

souce bashrc

echo -n "$clear"
echo -n "$'\033[31m'"

cat <<'EOM'
               ___
              | __|__ _ ____  _
              | _|/ _` (_-< || |
            __|___\__,_/__/\_, |_____              _           _
           | _ ) __ _ __| ||__/|_   _|__ _ _ _ __ (_)_ _  __ _| |
           | _ \/ _` (_-< ' \    | |/ -_) '_| '  \| | ' \/ _` | |
           |___/\__,_/__/_||_| _ |_|\___|_| |_|_|_|_|_||_\__,_|_|
               |  \/  |__ _ __| |_ ___ _ _
               | |\/| / _` (_-<  _/ -_) '_|
               |_|  |_\__,_/__/\__\___|_|      [95mv1.0.0  [34m@rwxrob

EOM

declare -i major="${BASH_VERSION:0:1}"
if [[ ! ( $major == "4" || $major == "5" ) ]]; then
  echo "Sorry, requires Bash 4+. Install and try again."
  return
fi

# TODO clean this up once we have all the components ported to bash
# modular scripts

# source bashrc.d/100-platform-detection.bash
# source bashrc.d/102-path.bash
# source bashrc.d/104-shopt.bash
# source bashrc.d/106-temp.bash
# source bashrc.d/108-shortcuts-dir.bash


telln <<'EOM'
  Setup the terminal for Bash, Vim, Tmux, Lynx, Mutt.
  Please install thoses from the install script and config.
EOM

echo ""

################################## Bash ##################################

# dotfiles=(
    # $PWD/bashconfig:$HOME/.bash_aliases
    # $PWD/vimrc:$HOME/.vimrc
    # $PWD/tmux.conf:$HOME/.tmux.conf
    # $PWD/alacritty.yml:$HOME/.alacritty.yml
    # $PWD/settings_vscode.json:$HOME/.config/Code/User/settings.json
# )
#
# for dotfile in ${dotfiles[@]}; do
    # key="${dotfile%%:*}"
    # value="${dotfile##*:}"
#
    # ln -sf $key $value
    # ls -l $value
# done


declare -A links=(
  [inputrc]=inputrc
  [bashrc]=bashrc
  [fonts]=fonts
  [bashrc]=bashrc
  [bash_profile]=profile
  [profile]=profile
  [dircolors]=dircolors
)

tell '**Setup Bash?**'

if confirm; then

for from in "${!links[@]}"; do
  symlink "$HOME/.$from" "$PWD/${links[$from]}"
done

echo ""

remindln <<'EOM'
Don't forget to create a `.bash_personal` and/or a `.bash_private`,
which should usually be from their own repos. See the following repos
You might also want to create symlinks from ~/.config subdirectories
to your own private repo directories in order to preserve your
application settings (ex: ~/.config/inkscape -> $PRIVATE/inkscape).
EOM

echo ""

fi

########################### Screen-centric TMUX ##########################

# tell '**Setup TMUX?**'
#
# if confirm; then
  # type tmux
  # if [[ $? == 0 ]]; then
    # symlink "$HOME/.tmux.conf" "$PWD/tmux.conf"
  # else
    # warnln "TMUX isn't installed. Skipping."
  # fi
    # echo
# fi

################################## More ##################################

# TODO add an interactive prompt for this stuff

# mkdir -p "$HOME/.config/alacritty"
# symlink "$HOME/.config/alacritty" "$PWD/alacritty"
#
# mkdir -p "$HOME/.config/lynx"
# symlink "$HOME/.config/lynx" "$PWD/lynx"
#

echo ""

telln <<'EOM'
  **All done.** Type the following command to reset your currently running
  bash terminal session to a new one using all your new setting. In fact,
  remember this command. It will save lots of time resetting every time
  you make a change in the future without closing your terminal and
  starting a new one.

    `exec bash`
EOM

echo ""


