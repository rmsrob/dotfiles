# Neovim nightly


sudo apt-get install ninja-build gettext libtool libtool-bin autoconf automake cmake g++ pkg-config unzip


```sh
wget https://github.com/neovim/neovim/releases/download/nightly/nvim-linux64.tar.gz
```

alias nvim='$HOME.nvim-linus64/bin/nvim'



ln -sv /home/rems/Repos/gitlab.com/rmsrob/dotfiles/config/nvim/init.vim ~/.config/nvim/init.vim
ln -sv /home/rems/Repos/gitlab.com/rmsrob/dotfiles/config/nvim/lua ~/.config/nvim/lua


:PackerInstall
:TSInstall all
-- LspInstall all


# Delete
rm -r /home/rems/.local/share/nvim
