local g = vim.g

g.closetag_filenames = { '*.html','*.xhtml','*.phtml','*.svelte' }
g.closetag_xhtml_filenames = { '*.xhtml','*.jsx','*.js','*.ts','*.tsx' }
g.closetag_filetypes = { 'html','xhtml','phtml','javascript','typescript' }

