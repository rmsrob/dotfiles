local g = vim.g

g.tagalong_filetypes = {'html', 'xml', 'jsx', 'tsx', 'eruby', 'ejs', 'eco', 'php', 'htmldjango', 'javascriptreact', 'typescriptreact', 'javascript'}
g.tagalong_verbose = true
